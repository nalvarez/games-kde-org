---
layout: page
sorted: 2
permalink: /news/
pagination: 
  enabled: true
  category: news
  sort_reverse: true
---
<link rel="stylesheet" type="text/css" href="https://cdn.kde.org/breeze-icons/icons.css">

<h1>The KDE Games Center</h1>  
<i class="icon icon_rss"></i> <a href="/feed.xml">Subscribe to our news feed</a>

{% include blog.html %}