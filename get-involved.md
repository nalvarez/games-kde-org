---
layout: page
title: Get Involved
konqi: /assets/img/konqi-dev.png
sorted: 5
---

# Get Involved!

Want to make KDE Games better? Consider getting involved in KDE Games development
and help us make KDE Games the best irc client!

## Build KDE Games from Source

The [community wiki](https://community.kde.org/Get_Involved/development)
provides excellent resources for setting up your very own development
environment.

## Get in Touch!

Most development-related discussions take place on the [kde-games-devel mailing
list](http://mail.kde.org/mailman/listinfo/kde-games-devel). Just join in, say hi
and tell us what you would like to help us with! You can also chat with us on
[#kde-games](irc://chat.freenode.net/kde-games) on Freenode, but this channel is
not very active.

## Not a Programmer?

Not a problem! There's plenty of other tasks that you can help us with to
make KDE Games better, even if you don't know any programming languages!

* [Bug triaging](https://community.kde.org/Guidelines_and_HOWTOs/Bug_triaging) - help us find
  mis-filed, duplicated or invalid bug reports in Bugzilla
* [Localization](https://community.kde.org/Get_Involved/translation) - help to translate
  KDE Games into your language
* [Documentation](https://community.kde.org/Get_Involved/documentation) - help us improve user
  documentation to make KDE Games more friendly for newcomers
* [Promotion](https://community.kde.org/Get_Involved/promotion) - help us promote KDE Games
  both online and offline
* [Updating wiki](https://userbase.kde.org/KDE Games) - help update the information present in
  the wiki, add new tutorials, etc. - help make it easier for others to join!
* Do you have any other idea? Get in touch!

