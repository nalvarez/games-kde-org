<ul>
<li>If you are using SUSE 10.3 please use the One-Click Install link below:<br /><br /></li>
<li><a href="http://software.opensuse.org/ymp/openSUSE%3A10.3/standard/kdegames4.ymp">SuSE10.3 One-Click Install.</a><br /><br /></li>
<li>If you are on another version of SUSE, or wish to use <b>Yast</b> instead, check out the following 
page on SUSE official website for instructions: <a href="http://en.opensuse.org/KDE4">KDE4 installation instructions</a></li>
</ul>
