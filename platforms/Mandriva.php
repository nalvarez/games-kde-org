<ul><li>Please note – The following packages are intended to be used under Mandriva 2008, and are not guaranteed to perform under any other version.<br /><br /></li>
<li>Please choose your hardware architecture using links below:<br /><br /></li>
<li>Mandriva 2008 - <a href="http://kde-mirror.freenux.org/stable/4.0.0/Mandriva/2008.0/RPMS/i586/kdegames4-4.0.0-1mdv2008.0.i586.rpm">i586 version.</a></li>
<li>Mandriva 2008 - <a href="http://kde-mirror.freenux.org/stable/4.0.0/Mandriva/2008.0/RPMS/x86_64/kdegames4-4.0.0-1mdv2008.0.x86_64.rpm">x86-64 version.</a></li>
</ul>