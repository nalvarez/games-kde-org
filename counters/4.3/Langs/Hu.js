Langs['4.3/Counter']['Banner'] = Array('','Egyszerű és átlátható konfiguráció','A fájlkezelés jobb, mint valaha','Könnyebb fotó- és képnézegetés','A Web ereje az ujjaid alatt','Sokkal jobb kereső és alkalmazásindító','Fantasztikus kommunikációs megoldások','Élvezetesebb használat, mint valaha','Több, mint 60.000 frissítés','Több, mint 2.000 új lehetőség');
Langs['4.3/Counter']['Square'] = Array('','Konfigurálhatóság','Fájlkezelés','Képnézegetés','Web2.0-ás böngészés','Sokkal jobb menük','Kommunikáció','Jobb asztal','60.000 frissítés','2.000 új lehetőség');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'A KDE 4.3 kiadásáig még hátra van %s nap';
	Langs['4.3/Counter']['Square'][0]  = '%s nap van hátra';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'A KDE 4.3 ma jelenik meg';
	Langs['4.3/Counter']['Square'][0]  = 'A kiadás napja';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'A KDE 4.3 megjelent';
	Langs['4.3/Counter']['Square'][0]  = '4.3 megjelent';
}