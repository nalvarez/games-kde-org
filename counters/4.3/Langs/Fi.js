Langs['4.3/Counter']['Banner'] = Array('','Yksinkertaiset ja intuitiiviset asetukset','Entistä parempi tiedostonhallinta','Kuvienkatselusta on tehty helppoa','Verkon tehokkuus sormenpäissäsi','Paranneltu haku ja ohjelmakäynnistin','Fantastiset kommunikointiratkaisut','Työpöytäkokemus ennennäkemättömällä tasolla','Yli 60 000 päivitystä','Yli 2 000 uutta ominaisuutta');
Langs['4.3/Counter']['Square'] = Array('','Muokattavuus','Tiedostonhallinta','Kuvankatselu','Web 2.0 -selailu','Parannellut valikot','Kommunikaatio','Parempi työpöytä','60 000 päivitystä','2 000 uutta ominaisuutta');
if (HowMany > 1) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE-yhteisö julkaisee KDE 4.3:n %:n päivän kuluttua';
	Langs['4.3/Counter']['Square'][0]  = '%s päivää jäljellä';
} else if ( HowMany == 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 julkaistaan tänään';
	Langs['4.3/Counter']['Square'][0]  = 'Julkaisupäivä';
} else if ( Howmany < 1 ) {
	Langs['4.3/Counter']['Banner'][0]  = 'KDE 4.3 on julkaistu';
	Langs['4.3/Counter']['Square'][0]  = '4.3 on julkaistu';
}