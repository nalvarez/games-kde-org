---
layout: post
date: 2007-11-20
title: New, redesigned website!
category: news
---

It is with a great pleasure that I wish to inform you of our website redesign near completion.

Yes, only a few minor “cosmetic” touches and the sleeping beauty shall gracefully awaken and present itself to the world. None of this would have been possible without Emil, our faithful web engineer who stepped in to upgrade the ancient Capacity's code, making sure it is suitable for the new era of KDE. And, as you can see the result is rather pleasing.

Now, please stay tuned as we unveil the next set of exiting new features in the nearest future. And remember **KDEGames** always strives for the best!