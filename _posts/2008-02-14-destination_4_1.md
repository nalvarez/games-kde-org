---
layout: post
date: 2008-02-14
title: Destination - 4.1
category: news
---

Lately quite a few people have been asking about the new games and features planned for 4.1
Well, KDEGames team have spent the whole month of January planing and scheduling, and now we are more, or less set. As games of the project are numerous however, we have so decided to concentrate on a selected few, rather then spreading our attention thin-and-even around the project.

<img class="news-img text-center" src="/pics/kdegames.png">

Kgoldrunner will receive a lot of attention within this release cycle, where new theming capabilities are being added as you read. There are also plans to introduce the long awaited sound and make the game all over fun and joy (as if it wasn't joyous enough as is).

Kpatience and consequently Letenant Skat are getting a major caching rework, which promises to make those games even faster to load and play, as some of the card decks are a real handful to handle.

Klines is getting some user interface changes that promises to somewhat enhance the game's usability and bring a gust of fresh air into the gameplay.

Konquest development is swift as ever. Game's restless developer is hard at work this release cycle, hoping to make our best, desktop-oriented mini-strategy-game even better.

There are currently plans for some on-line gaming and sound integration all over the project. Those however, are much dependent upon other parts of KDE project and therefore may not be ready by 4.1 release date.

Beside the existing gaming applications there is a handful of new, never before seen games, ready to jump up the KDE locomotive as soon as 4.1 release sees the light of day.

We have an all new Tetris game, which is not a resurrection of severely outdated Ksirtet, but a whole new game all together. Once complete it will feature easy and scalable theming, and gamer-friendly interface to go along. The game is already playable and should be finished in time.

As if Tetris wasn't exciting enough, we have yet another tasty dish to tickle your senses! If you are currently standing, you would better take a seat, because it's a mind-blowing Arkanoid game! Nearly completed, it features all you have come to love about the classic Arcanoid game, and much, much more.

Now, get ready for yet another big surprise! Kdiamond will make you jump up-and-down in your seat, because it's a stunning Bejeweled implementation. This game however, may, or may not be ready by 4.1 as the development was only just started, even though the developer is advancing through at the speed of light.

Another candidate aiming at 4.1 release is the long time KDE favorite Ksirk - the Game of Risk implementation. It is being reworked and updated for quite a while, and now is almost ready to join the official KDE games pack.

A Rubik's cube-like game is planing to land safely into the 4.1 release and will of course be most welcome.

We have plans for a Picross-like puzzle game, but there are no dates named yet for this one.

There are two more games currently hard at work, but those are more of a big-birthday-surprise-cake type of games. Let us give you a small hint here. The first is quite simple, yet so addictive that should you try it just once, you will never be able to stop playing it. As for the second – let me just say that once you see it you are guaranteed to have a severe case of jaw dropping for the next two-three hours. Yes Ladies and Gentlemen, it will be that awesome! And did I mention massive explosions?

Alas, as you may have already figured, future holds a lot in terms of quality KDE entertainment. Just keep checking up on us, and keep playing.