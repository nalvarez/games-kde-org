---
title: KSudoku
name: ksudoku
layout: game
category: logic
version: 0.5
credits:
  year: 2005
  license: GPL-2.0-only
authors:
  - name: Francesco Rossi
    email: redsh@email.it
    maintainer: true

  - name: Johannes Bergmeier
    email: Johannes.Bergmeier@gmx.net
    maintainer: true

  - name: Mick Kappenburg
    email: ksudoku@kappendburg.net
    maintainer: true

description: 
  KSudoku is a logic-based symbol placement puzzle. The player has to fill a grid so that each column, row as well as each square block on the game field contains only one instance of each symbol.

howto: |
  **Objective:** *Fill the grid so that each column, row as well as each square sector on the game field contains only one instance of each symbol.*
  
  At the game start you are prompted to choose which game type you are interested in.

  **Note**: *The game experience may change slightly depending on the game type you choose.*

  Take a look at left hand side of the game field. There is a selection list which contains all the symbols available for you to use in the current game.

  **Note**: The set of **symbols** varies depending on the game type you choose.

  First, note the boundaries of the game field. The thick black lines outline the sectors. Every one of this sectors has to be completed using the **symbols** available, in such a way that each **symbol** is used within the sector area once and only once.

  Now you can start entering the **symbols** into the vacant squares on the game field. You can do that by selecting the **symbol** you wish to enter from the selection list on the left, and then using your mouse to click on the vacant square on the game field.

  Notice that the squares change color while you hover your mouse over them. It is done to help you keep up with the rules of **KSudoku**. Use the vertical and horizontal lines to check if the **symbol**, you are about to enter into the vacant spot, is already in use anywhere in the vertical or horizontal line that square is a part of. The coloring also highlights the sector which, if you remember, must also contain only one instance of each **symbol**.

  Keep entering the **symbols** in accordance with the riles until the whole game field is filled up. At this pint the application will check if all the entries are valid and either correct you, or except the solution.
---
