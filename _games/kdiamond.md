---
title: KDiamond
name: kdiamond
layout: game
category: arcade
version: 1.4
credits:
  year: 2008
  license: GPL-2.0-only
authors:
  - name: Stefan Majewsky
    email: majewsky@gmx.net
    maintainer: true

  - name: Paul Bunbury
    email: happysmileman@googlemail.com
    maintainer: true

contributors:
  - name: Eugene Trounev
    email: eugene.trounev@gmail.com

  - name: Felix Lemke
    email: lemke.felix@ages-skripte.org

  - name: Jeffrey Kelling
    email: kelling.jeffrey@ages-skripte.org

description: |
  KDiamond is a single player puzzle game.
  The object of the game is to build lines of three similar diamonds.

howto: |
  The player is presented with a rectangular grid containing several types of diamonds. The object of the game is to swap neighbored diamonds to assemble a line of three similar diamonds. These lines will vanish and the board will be refilled with new diamonds. The game time is limited, try to build as much lines as possible to earn a high score.

  Click on one diamond, then on one of its neighbors. The diamonds will then be swapped. If no line of at least three diamonds is formed by the swap, it will be reverted. The status bar displays the game time left, and the number of points you have earned.
---
