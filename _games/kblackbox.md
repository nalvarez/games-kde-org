---
title: KBlackbox
name: kblackbox
layout: game
category: logic
version: 0.4.0
credits:
  year: 1999
  license: GPL-2.0-only
authors:
  - name: Nicolas Roffet
    email: nicolas-kde@roffet.com
    maintainer: true

  - name: Robert Cimrman
    email: cimrman3@students.zcu.cz
    maintainer: true

contributors:
  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

description: 
  KBlackbox is a game of hide and seek played on a grid of boxes where the computer has hidden several balls.

howto: |
  **Objective:** *Use laser beams to deduce the placement of the atomic particles inside the black box.*
  
  Using lasers and placing balls and markers deduce the position of the hideden atoms inside the black box.

  The cursor can be moved around the box with the standard cursor movement keys or the mouse. Switching of lasers or marking of black boxes is done with the **Left Mouse Button**, or by pressing the **Return** or **Enter** key.

  You can also drag and drop the balls and the markers with the mouse.

  You can mark the fields where you think a ball cannot be, too. Just press the **Right Mouse Button**. It often helps you to find an area where a ball could possibly be. To clear any marks, press the same key or mouse button again.

  When you think the configuration of balls you have placed is correct, press the "Done!" button. You will be informed whether you are correct or not, and be given your score.

  If you placed any balls incorrectly, the right solution will be displayed.
---
