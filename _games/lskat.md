---
title: LsKat
name: lskat
layout: game
category: card
version: 1.40
credits:
  year: 2000
  license: GPL-2.0-only
authors:
  - name: Martin Heni
    email: kde@heni-online.de
    maintainer: true

  - name: Eugene Trounev
    email: eugene.trounev@gmail.com
    maintainer: true

  - name: Benjamin Meyer 
    maintainer: true

description: |
  Lieutnant Skat (from German "Offiziersskat") is a fun and engaging card game for two players, where the second player is either live opponent, or built in artificial intelligence. You can also play and test an [online demo version of this game](http://www.heni-online.de/java/lskatplay_js.php). 

howto: |
  **Objective:** *Score more then 60 points in a single game.*
  
  Once dialing is finished each player ends up with 16 **cards** each. **Cards** are displayed in two vertical rows - four **cards** per row. Half of the cards are face down and half is face up. As a player you can see the opponent's **cards** as well as your own. You can make a move then by clicking on any of your **cards** which are currently face up. The opponent has to reply by choosing a corresponding card form his/her own stack.

  **Note**: *If your opponent is artificial intelligence, it's cards will be chosen automatically.*

  Once the exchange is finished values of the played **cards** are evaluated and either you or your opponent is awarded points. The game is complete in 16 rounds *(when all of your and your opponent's cards have been played)*. After the game ends your score is be compared to that of your opponent's and the player with the most sore wins the round.
---
