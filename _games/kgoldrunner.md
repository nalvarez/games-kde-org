---
title: KGoldrunner
name: kgoldrunner
layout: game
category: arcade
version: 4.6
credits:
  year: 2003
  license: GPL-2.0-only
authors:
  - name: Ian Wadham
    email: ianw2@optusnet.com.au
    maintainer: true

  - name: Marco Krüger
    email: grisuji@gmx.de
    maintainer: true

contributors:
  - name: Mauricio Piacentini
    email: mauricio@tabuleiro.com

  - name: Maurizio Monge
    email: maurizio.monge@gmail.com

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Luciano Montanaro
    email: mikelima@cirulla.net

description: |
  KGoldrunner is a maze-threading game with a puzzle flavor. It has hundreds of levels where pieces of gold must be collected, with enemies in hot pursuit. 

howto: |
  You control a **hero** who must collect all the **gold** in the level while avoiding enemies trying to catch him.
  
  Each level has a different maze layout. The mazes use a multi-story platform scheme, with ladders and hand-to-hand bars for climbing. There are usually several ways to travel through the maze.

  You can dig holes in the floor and temporarily trap **enemies**. The **hero** may safely walk on top of an enemy trapped in a hole. After a while, the floor regenerates. If a hole fills up before an enemy climbs out, he is consumed and then reappears at another place in the maze.

  The **hero** and **enemies** can fall from any height without being injured, but cannot jump upwards, so it is possible for the **hero** to become trapped in a pit and then the player must kill the **hero**. The game starts with five lives and each level completed earns an extra life.

  Certain parts of the floor are trapdoors through which the player and **enemies** will fall and other parts are rocks which cannot be dug. The player finishes a level by collecting all the **gold** and then travelling to the top of the screen.
---
