---
title: Kollision
name: kollision
layout: game
category: arcade
version: 0.1
credits:
  year: 2007
  license: GPL-2.0-only
authors:
  - name: Paolo Capriotti
    email: p.capriotti@gmail.com
    maintainer: true

  - name: Dmitry Suzdalev
    email: dimsuz@gmail.com
    maintainer: true

contributors:
  - name: Matteo Guarnieri
    email:

description: |
  A simple ball dodging game.

howto: |
  Click on the empty field to start a game: a blue ball immediately replaces your mouse cursor, and a number of red balls start to fade into the field.

  When the red balls finish materializing, they move at a random speed in a random direction, and you have to prompty dodge them moving the blue ball inside the field with your mouse. After some time, other red balls will appear, fading in as before and then starting to move. Be prepared to avoid them, too.
---
