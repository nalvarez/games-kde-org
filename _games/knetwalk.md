---
title: KNetwalk
name: knetwalk
layout: game
category: logic
version: 3.0.0
credits:
  year: 2004
  license: GPL-2.0-only
authors:
  - name: Andi Peredr
    email: andi@ukr.net
    maintainer: true

  - name: Thomas Nagy
    email: tnagyemail-mail@yahoo.fr
    maintainer: true

  - name: Fela Winkelmolen
    email: fela.kde@gmail.com
    maintainer: true
    
contributors:
  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

description: 
  KNetwalk is a single player logic game. Construct the network, connecting all the terminals to the server in the fewest turns possible. 

howto: |
  The player is presented with a rectangular grid containing a **server**, several terminals (clients), and pieces of **wire**. The object of the game is to rotate these elements until every client is **connected** to the **server**, and no **wires** are left unconnected. Optionally, you can try to get a **high score** by minimizing the number of rotations needed.

  Left clicking on a square rotates it counterclockwise, while right clicking rotates it clockwise. Middle clicking on a square marks it as blocked and prevents it from being rotated unless it gets unblocked by another middle click; this can be very useful for marking squares which are known to be in the correct orientation. The status bar displays the number of clicks so far.
---
