---
title: Konquest
name: konquest
layout: game
category: strategy
version: 2.0
credits:
  year: 1999
  license: GPL-2.0-only
authors:
  - name: Russell Steffen
    email: rsteffen@bayarea.net
    maintainer: true
    
contributors:
  - name: Stephan Zehetner
    email: s.zehetner@nevox.org

  - name: Dimitry Suzalev
    email: dimsuz@gmail.com

  - name: Inge Wallin
    email: inge@lysator.liu.se

  - name: Pierre Ducroquet
    email: pinaraf@gmail.com

  - name: Eugene Trounev
    email: irs_me@hotmail.com

  - name: Anton Brondz
    email: dinolinux@gmail.com

  - name: Johann Ollivier Lapeyre
    email: johann.ollivierlapeyre@gmail.com

description: 
  Konquest is the KDE version of Gnu-Lactic Konquest. Players conquer other planets by sending ships to them. The goal is to build an interstellar empire and ultimately conquer all other player's planets.

howto: |
  When you start Konquest, press New Game to start a new game you will see a dialog in which you need to enter player names, decide the number of **planets**, and how many turns are allowed. You can also use the reject button to get a new map. When you're done, click OK to start the game.

  Once the game starts, you will be presented with the game board. Blank squares are empty space, while filled squres identify the existing **planets**.

  At first, each player owns one **planet**. The **planet's** background is the player color. If you move your mouse over a **planet**, you can view additional information. On **planets** which have not yet been conquered by anyone, you will only see the **planet's** name. The information includes:

  - Planet name
  - Owner
  - Ships
  - Production
  - Kill percent

  The name of the **planet** is how the **planet** is referred to in the game. Planets are named alphabetically with uppercase letters.

  Owner is the player who owns the **planet**. If the **planet** is conquered by another player, this will change.

  Ships are the current number of ships on the **planet**.

  Production is the number of ships the **planet** will produce each turn.

  The production varies from **planet** to **planet**, but always remains the same.

  Kill percent is a measure of the effectiveness of the ships produced at that **planet**.

  Attack fleets take the kill percentage of their **planet** of departure, and defense fleets use the kill percentage of the **planet** they are defending.

  You can easily see the **planet** name, the owner and the current number of ships on a **planet** in the square in which the **planet** lies. The owner is represented by the background color of the **planet**, the name is in the upper left corner and the number of ships is in the lower right corner.

  To send ships from one **planet** to another, select the **planet** you want to send ships from, enter the number of ships in the green box in the upper right corner and press enter. Repeat this procedure until you have sent out all the ships you want. When you're done, press End Turn. When all players have played one turn, dialog boxes will appear and give you the latest news. The game will proceed like this until one of the players owns the entire galaxy.
---
