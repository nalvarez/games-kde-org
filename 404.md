---
layout: page
permalink: /404.html
---

<center>
<img src="/assets/img/404.svg" style="height: 20rem;">	
<h1 style="font-size: 4rem; margin-top: 1rem;"> Error 404 </h1>
<h4>Game Not Found!</h4>
</center>