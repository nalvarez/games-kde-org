<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KReversi </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kreversi.png" ALT="KReversi" BORDER="0" ALIGN="right" WIDTH="97" HEIGHT="120">
      <em>KReversi</em> is a board game game where two players have
      to gain the majority of pieces on the board. This is done by tactically
      placing ones pieces to turn over the opponents pieces.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:mweilguni@NO__SPAMsime.com">Mario Weilguni</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.2.1 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

