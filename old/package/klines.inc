<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KLines </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/klines.png" ALT="KLines" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="110">
      <em>Klines</em> is the KDE version of the russian game <em>Lines</em>
      where you have to align five game pieces of the same colour in one line
      to remove them from the game board. Similar to tetris you fight new
      pieces appearing.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:Roman.Razilov@NO__SPAMgmx.de">Roman Razilov</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.0 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

