<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> Lieutnant Skat </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/lskat.png" ALT="lskat" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="94">
          <em>Lieutenant Skat</em> is a nice two player card game which follows
          the rules for the German game <em>(Offiziers)-Skat</em>.
          The program includes many different
          carddecks to choose. A computer opponent can play for any of
          the players.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Author:
    </td><td valign=top>
        <a href="mailto:martin@NO__SPAMheni-online.de">Martin Heni</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a href="http://www.heni-online.de/linux/index.html#lskat">www.heni-online.de/linux</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.91 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

