<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KSirtet </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ksirtet.png" ALT="KSirtet" BORDER="0" ALIGN="right" WIDTH="97" HEIGHT="121">
      <em>KSirtet</em> is a clone of the
      well known <em>Tetris</em> game family.
      The game allows multiplayer duels including
      games against a computer player. Everybody
      knowing <em>Tetris</em> should immediately
      feel at home with this game.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        Eirik Eng<br>
        <a href="mailto:hadacek@NO__SPAMkde.org">Nicolas Hadacek</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a href="http://ksirtet.sourceforge.net">ksirtet.sourceforge.net</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v2.1.4 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1-2 player <br> 
        <img src="./images/computer.png" border="0"  height="24" width="24" alt="computerplayer">
        &nbsp;
        <img src="./images/network.png" border="0"  height="24" width="24" alt="network game">
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

