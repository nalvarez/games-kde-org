<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> Shishen-Sho </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kshishen.png" ALT="KShishen" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="101">
      <em>Shisen-Sho</em> (<em>KShishen</em>) is a game similiar to Mahjongg.
      The object of the game is to remove all tiles from the field. This is
      done by removing two tiles with of the same type until no tile is left.
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:mweilguni@NO__SPAMsime.com">Mario Weilguni</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v1.3 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

