<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3>KTuberling </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/ktuberling.png" ALT="KTuberling" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="95">
          <em>KTuberling</em> is a nice potato editor for kids.
          The game intended for small children. Of course, it may be suitable for adults
          who have remained young at heart. 
          Eyes, mouths, mustache, and other parts of face and goodies can
          be attached onto a potato-like guy. 
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Author:
    </td><td valign=top>
        <a href="mailto:e.bischoff@NO__SPAMnoos.fr">Eric Bischoff</a> 
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.3.3 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

