<!--  NEW SECTION -->  
<TABLE align="center" WIDTH="100%" BGCOLOR="#000080" CELLPADDING="0" CELLSPACING="0" BORDER="0"><tr><td>
 <table width="100%"  CELLSPACING="1" CELLPADDING="3" BORDER="0" BGCOLOR="#f0f0ff" ><tr><td>
    
  <h3> KMahjongg </h3>

  <table><tr><td>
    <p>
    <IMG SRC="images/kmahjongg.png" ALT="KMahjongg" BORDER="0" ALIGN="right" WIDTH="120" HEIGHT="113">
      <em>KMahjongg</em> is a clone of the well known tile based patience game
      of the same name. In the game you have to empty a game board
      filled with piece by removing pieces of the same type. 
    </p>
  </td>
  </tr></table>
  <p>

  <table>
    <tr><td valign=top>
       Authors:
    </td><td valign=top>
        <a href="mailto:in5y158@NO__SPAMpublic.uni-hamburg.de">Mathias Mueller</a> 
    </td></tr>

    <tr><td valign=top>
       Homepage:
    </td><td valign=top>
        <a href="kmahjongg/index.html">games.kde.org/kmahjongg</a>
    </td></tr>

    <tr><td valign=top>
       Version:
    </td><td valign=top>
	    v0.7.2 - KDE 3.x
    </td></tr>

    <tr><td valign=top>
       Game Info:
    </td><td valign=top>
       1 player <br> 
    </td></tr>


  </table>
 </td></tr></table>
</td></tr></table>
&nbsp;<br>

