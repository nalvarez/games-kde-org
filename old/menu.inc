<?php
$this->setName ("KDE Games Center");

$section =& $this->appendSection("General");
$section->appendLink("Games Center","");
$section->appendLink("kde.org","http://www.kde.org/",false);
$section->appendLink("New Games","..",false);

$section =& $this->appendSection("Package");
$section->appendLink("Arcade Games","kde_arcade.php");
$section->appendLink("Boardgames","kde_boardgames.php");
$section->appendLink("Card Games","kde_cardgames.php");
$section->appendLink("Tactic&amp;Strategy","kde_tactics.php");
$section->appendLink("Kids Games","kde_kids.php");

$section =& $this->appendSection("Info");
$section->appendLink("Editor's Choice","othergames.php");
$section->appendLink("Top 10 Games","top10.php");
$section->appendLink("kde-apps.org","http://kde-apps.org",false);

$section =& $this->appendSection("Develop");
$section->appendLink("Articles&amp;Docs","gamedoc.php");
$section->appendLink("KDE&amp;Qt Books","http://www.kde.org/stuff/books.php",false);
$section->appendLink("Tools&amp;Utilities","tools.php");
$section->appendLink("People","people.php");

?>
