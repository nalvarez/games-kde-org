<?php
  $page_title = "KDE Games Documentation";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

      <p>
		  <font size="+1">T</font>his section contains links to interesting
      articles and documentation for games. This includes principle
      articles like artificial intelligence as well as documentation
      of libraries like the <em>libkdegames</em>.
      </p>

      <p>
      In this section we depend on <b>your</b> help, too. Please send
      <a href="mailto:martin@NO__SPAMheni-online.de">us</a> links
      to articles you found yourself useful and think they could
      be listed here.
      </p>

      <!-- *********************************************************** -->

    <dl>
    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://edu.kde.org/developer//kaction.phtml"
    >edu.kde.org</a>
    </dt>
    <dd>
      Explains the usage of <em>KActions</em> and <em>KAccels</em>
      in KDE games
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://www.gameai.com/ai.html"
    >www.gameai.com</a>
    </dt>
    <dd>
      Using computer artifical intelligence in games
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://theory.stanford.edu/~amitp/GameProgramming/"
    >theory.stanford.edu</a>
    </dt>
    <dd>
      Path finding algorithms in game applications
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://www.gamedev.net/reference/list.asp?categoryid=30"
    >www.gamedev.net</a>
    </dt>
    <dd>
    Multiplayer and network game design
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://www.gamedev.net/reference/list.asp?categoryid=44"
    >www.gamedev.net</a>
    </dt>
    <dd>
      Tiling in games and tile based games
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://zez.org/article/articleview/2/"
    >zez.org</a>
    </dd>
    <dd>
    The usage of <em>QCanvas</em> and <em>QCanvasSprites</em> in Qt games
    <br>
    &nbsp;
    </dd>

    <dt>
&nbsp;<?php echo ("<img src=\"images/away.png\" align=\"bottom\" alt=\"away link\" border=0>"); ?>
    &nbsp;<a href="http://sunsite.dk/lgdc/"
    >sunsite.dk</a>
    </dt>
    <dd>
    The Linux Game Center resources
    <br>
    &nbsp;
    </dd>

      </dl>

<hr>

<?php
INCLUDE "nospam.inc";
?>

&nbsp;<p>
&nbsp;<p>
&nbsp;<p>
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update:
    <?php echo (date("dS F Y",getlastmod()).", <a href=\"mailto:".$mail."\">".$author."</a>\n"); ?>
</font>

<?php
INCLUDE "footer.inc";
?>
