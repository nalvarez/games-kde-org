<?php
  $page_title = "Kdegames Package: Tactic&Strategy Games";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

      <FONT SIZE="+1">I</FONT>n this section we present information about the
      <em>Tactic&Strategy Games</em> included in KDE. All games listed
      here are part of the kdegames package and included in a standard
      KDE distribution.
      <p>
      &nbsp;
      <p>

    <TABLE align="center" border="0" width="90%"><TR><TD>
    <?php
      include ("package/katomic.inc");
      include ("package/kjumpingcube.inc");
      include ("package/kmines.inc");
      include ("package/ksokoban.inc");
      include ("package/klines.inc");
      include ("package/konquest.inc");
      include ("package/ksame.inc");
    ?>

    </TD></TR></TABLE>
    <p>

    <?php
      include ("package/legend.inc");
    ?>
    <p>
    &nbsp;
    <p>

    For comments and critics please contact
    <A HREF="mailto:martin@NO__SPAMheni-online.de">Martin Heni</A>.

<hr>
<?php
INCLUDE "nospam.inc";
?>
&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a
 href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>

		  
