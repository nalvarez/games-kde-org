<?php
  $page_title = "Kdegames Package: Boardgames";
  include "games.inc";

  $author="M. Heni";
  $mail="martin@NO__SPAMheni-online.de";
  include "header.inc";
?>

      <FONT SIZE="+1">I</FONT>n this section we present information about the
      <em>Board Games</em> included in KDE. All games listed
      here are part of the kdegames package and included in a standard
      KDE distribution.
      <p>
      &nbsp;
      <p>

    <TABLE align="center" border="0" width="90%"><TR><TD>
    <?php
      include ("package/kbackgammon.inc");
      include ("package/kbattleship.inc");
      include ("package/kblackbox.inc");
      include ("package/kabalone.inc");
      include ("package/kiriki.inc");
      include ("package/kmahjongg.inc");
      include ("package/kreversi.inc");
      include ("package/kshishen.inc");
      include ("package/ksquares.inc");
      include ("package/kwin4.inc");
    ?>

    </TD></TR></TABLE>
    <p>

    <?php
      include ("package/legend.inc");
    ?>
    <p>
    &nbsp;
    <p>

    For comments and critics please contact
    <A HREF="mailto:martin@NO__SPAMheni-online.de">Martin Heni</A>.

<hr>
<?php
INCLUDE "nospam.inc";
?>
&nbsp;<p>     
&nbsp;<p>     
&nbsp;<p>     
<hr width=570 size=5 align="left" noshade>
<font size="-1">
	  Last update: 
    <?php echo (date("dS F Y",getlastmod()).", <a
 href=\"mailto:".$mail."\">".$author."</a>\n"); ?> 
</font>

<?php
INCLUDE "footer.inc";
?>

		  
